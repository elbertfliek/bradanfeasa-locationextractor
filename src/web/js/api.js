var bflocationextractor = (function () {

	var execute = function(cdef, config, cb, eh){
		$.ajax({
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			type: 'POST',
			url : '/bflocationextractor/api/v1.0/execute',
			data: JSON.stringify({'cdef':cdef, 'config':config}),
			success: cb,
			error: eh
		});
	}

	return {
		execute: execute
	}
})();
