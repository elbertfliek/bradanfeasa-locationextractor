#!/bin/bash

PROJECT="bflocationextractor"
FOLDER="bflocationextractor"

pip install -r /usr/lib/python2.7/$FOLDER/requirements.txt

chmod 755 /etc/init.d/$PROJECT

chown -R www-data:www-data /var/www/$PROJECT/

update-rc.d $PROJECT defaults

service nginx restart
service $PROJECT restart
