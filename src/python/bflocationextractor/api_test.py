import requests

server = 'http://rnd-demo-nl.sl-ams01.crowdynews.net/bflocationextractor'


def find_similar_tags(cdef):
	req = requests.post(server + '/api/v1.0/extract', json=cdef)
	print req.status_code
	print req.content


def to_cdef(urls):
	items = []
	for url in urls:
		items.append({'original': {'url': url}})
	cdef = {'cdef': {'items': items}, 'config': None}
	return cdef


def main():
	urls = [
		'http://www.hotels.nl/nl/deventer/hoteldeventer/',
		'http://www.tui.nl/vakantie/spanje/balearen/mallorca/ca-n-picafort/suneo-club-haiti/',
		'http://www.booking.com/hotel/cw/bed-amp-breakfast-curacao.html'
	]
	cdef = to_cdef(urls)
	find_similar_tags(cdef)

if __name__ == "__main__":
	main()
