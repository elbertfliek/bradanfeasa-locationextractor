import logging
from bs4 import BeautifulSoup
import urllib
import requests
from readability import Document

logger = logging.getLogger(__name__)


def find_location_from_url_fallback(url):
	logger.info('Using fallback location extraction')
	location = {'latitude': None, 'longitude': None}
	r = requests.get(url)
	html = r.text
	for line in html.splitlines():
		if 'latitude' in line:
			latitude = ""
			found_period = False
			for char in line:
				if char.isnumeric():
					latitude += char
				if ord(char) is 46 and len(latitude) > 0 and not found_period:
					latitude += char
					found_period = True
				if char.isspace():
					break
			location['latitude'] = latitude

		if 'longitude' in line:
			longitude = ""
			found_period = False
			for char in line:
				if char.isnumeric():
					longitude += char
				if ord(char) is 46 and len(longitude) > 0 and not found_period:
					longitude += char
					found_period = True
				if char.isspace():
					break
			location['longitude'] = longitude
	return location


def find_location_from_url(url):
	logger.info('Extracting location from %s' % url)
	r = urllib.urlopen(url).read()
	soup = BeautifulSoup(r, 'html.parser')
	location = {'latitude': None, 'longitude': None}
	metas = soup.findAll('meta')
	for meta in metas:
		meta_string = str(meta.attrs)
		if 'longitude' in meta_string:
			try:
				location['longitude'] = float(meta.get('content'))
			except:
				logger.warn('Could not extract longitude from url')
		if 'latitude' in meta_string:
			try:
				location['latitude'] = float(meta.get('content'))
			except:
				logger.warn('Could not extract latitude from url')
	logger.info('Found location: %s' % location)
	return location


def find_name_from_url(url):
	response = requests.get(url)
	doc = Document(response.text)
	return doc.title()


def extract(cdef, config):
	for item in cdef['items']:
		url = item['original']['url']
		location = find_location_from_url(url)
		name = find_name_from_url(url)
		item['xExtractedLocation'] = {'location': location, 'name': name}
	return cdef
