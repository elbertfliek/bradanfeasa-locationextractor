import sys
import os
import os.path
import logging
import logging.config
import codecs
import json
from flask import Flask, Response, request
from flask.ext.cors import CORS

import rndcoreutils.config
import rndcoreutils.nagios as nagios
from rndcoreutils.server_util import json_response

import bflocationextractor.api as api


def resolve_file(path):
	if os.path.isfile('./src' + path):
		return './src' + path
	if os.path.isfile('../../src' + path):
		return '../../src' + path
	if os.path.isfile(path):
		return path
	raise Exception('Could not resolve %s' % path)

logger = logging.getLogger(__name__)

config = rndcoreutils.config.config('/etc/bflocationextractor/config.json')

app = Flask(__name__)
CORS(app)


@app.errorhandler(500)
def internal_server_error(e):
	message = str(e)
	nagios.increment(nagios.critical, message)
	logger.exception(e)
	import traceback
	traceback.print_exc()
	return Response(
		json.dumps({'error': message}),
		mimetype='application/json'
	), 500


@app.errorhandler(404)
def not_found_error(e):
	message = str(e)
	nagios.increment(nagios.warning, message)
	logging.warn(e)
	import traceback
	traceback.print_exc()
	return Response(
		json.dumps({'error': message}),
		mimetype='application/json'
	), 404


@app.route('/api/nagios')
@json_response
def check_errors():
	return nagios.report()


@app.route('/api/v1.0/extract', methods=['POST'])
@json_response
def extract():
	data = request.get_json()
	return api.extract(data['cdef'], data['config'])


if __name__ == '__main__':
	'''
	Start the server
	'''
	sys.stdout = codecs.getwriter('utf8')(sys.stdout)
	sys.stderr = codecs.getwriter('utf8')(sys.stderr)

	logger.info('server started')
	app.run(port=config['service']['port'], debug=False, use_reloader=False, threaded=True)
