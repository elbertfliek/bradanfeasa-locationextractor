SHELL := /bin/bash

# INITIALIZATION
scaffold:
	make/init-git

init: scaffold
	make/project-variables

clean:
	make/clean

doc: init

# Setup
setup-dev:
	make/setup-dev

setup-build-deb:
	make/setup-build-deb

# DEVELOPEMENT
dev-activate:
	make/dev-activate


# TESTING
test: test-unit test-lint
test-precommit: test-unit
test-lint:
	make/test-lint
test-nose:
	make/test-nose

# PACKAGING
build-deb:
	make/build-deb
build-wheel:
	make/build-wheel

install-deb:
	make/install-deb
install-wheel:
	make/install-wheel

# JENKINS
test-jenkins: init
build-jenkins: init
deploy-jenkins: init
