#!/bin/bash

# bail out if no argument passed
if [ -z $1 ]; then
	exit
fi

# process line by line
while read LINE ; do
	LINECOUNT=$[LINECOUNT +1]
	# skip comment lines
	if [[ ${LINE} =~ ^# ]]; then
		continue;
	fi

	# specific checks per line number
 	case ${LINECOUNT} in
		# first line: required, min length 10 chars, max length 50 chars, at least 2 words
		1) if [ ${#LINE} -gt 50 ]; then
				MESSAGES[${LINECOUNT}]="Error ${LINECOUNT}: First line should be less than 50 characters in length."
			elif [ -z ${#LINE} ]; then
				MESSAGES[${LINECOUNT}]="Error ${LINECOUNT}: First line should not be empty."
			elif [ ${#LINE} -lt 10 ]; then
				MESSAGES[${LINECOUNT}]="Error ${LINECOUNT}: First line should at least be 10 characters in length."
			fi
			;;
		# second line: optional, should be blank
		2) if [ ${#LINE} -ne 0 ]; then
				MESSAGES[${LINECOUNT}]="Error ${LINECOUNT}: Second line should be empty."
			fi
			;;
		# other lines: optional, max length 72 chars
		*) if [ ${#LINE} -gt 72 ]; then
				MESSAGES[${LINECOUNT}]="Error ${LINECOUNT}: No line should be over 72 characters long."
			fi
	esac
done < $1

# check that file is at least N lines long
if [ ${LINECOUNT} -lt 1 ]; then
	MESSAGES[${LINECOUNT}]="Error: Commit message should be at least 1 line."
fi

if [ ${#MESSAGES[@]} -gt 0 ]; then
	printf '%s\n' "${MESSAGES[@]}"
	exit 1
fi
