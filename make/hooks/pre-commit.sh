# pre-commit.sh
git stash -q --keep-index

# Test prospective commit
echo Run tests before commit
# Eventually we would like to run make test
make test-precommit

RESULT=$?
git stash pop -q
[ $RESULT -ne 0 ] && exit 1
exit 0
